use serde::{Deserialize, Serialize};
use std::collections::HashSet;

use crate::issue::{IssueId, IssueState};
use crate::state::State;

#[derive(Debug, Clone, PartialEq, Deserialize, Serialize)]
pub enum LabelEdit {
    Add(String),
    Remove(String),
}

impl LabelEdit {
    pub fn label(&self) -> &str {
        match self {
            LabelEdit::Add(s) | LabelEdit::Remove(s) => s,
        }
    }
}

pub fn parse_label_edit(s: &str) -> Result<LabelEdit, String> {
    if s.is_empty() {
        return Err("empty label edit".to_string());
    }

    // FIXME panics if first char is more than one byte. check and return err.
    match s.split_at(1) {
        ("+", label) => Ok(LabelEdit::Add(label.to_string())),
        ("-", label) => Ok(LabelEdit::Remove(label.to_string())),
        (c, _) => Err(format!(
            "label edits should start with '-' or '+', got '{c}'"
        )),
    }
}

pub fn modify_labels(issue: &IssueId, edits: &[LabelEdit], allow_new_labels: bool) {
    let mut state = State::read("issues.ron");
    let labels: HashSet<String> = state
        .labels
        .iter()
        .map(|label| label.name.clone())
        .collect();
    let iter: Box<dyn Iterator<Item = _>> = if allow_new_labels {
        // Take all the edits no matter what.
        Box::new(edits.iter())
    } else {
        // Filter out edits that use unknown labels.
        // FIXME: warn
        Box::new(edits.iter().filter(|edit| labels.contains(edit.label())))
    };
    let issue = state.from_id_mut(issue).unwrap();
    iter.for_each(|edit| apply_label_edit(issue, edit));
}

pub fn apply_label_edit(issue: &mut IssueState, edit: &LabelEdit) {
    let is_add = matches!(edit, LabelEdit::Add(_));
    let is_remove = matches!(edit, LabelEdit::Remove(_));

    let label = match edit {
        LabelEdit::Add(label) => label,
        LabelEdit::Remove(label) => label,
    };

    let already_has = |search| {
        issue
            .label_edits
            .iter()
            .enumerate()
            .find_map(|(idx, edit)| edit.eq(&search).then_some(idx))
    };
    let already_has_add = already_has(LabelEdit::Add(label.to_string()));
    let already_has_remove = already_has(LabelEdit::Remove(label.to_string()));

    // Add the edit, taking care to
    // - not add duplicates
    // - removing changes that haven't been applied if they cancel out

    if is_add {
        if already_has_add.is_some() {
            // do nothing
        } else if let Some(idx) = already_has_remove {
            let _ = issue.label_edits.remove(idx);
        } else {
            issue.label_edits.push(edit.clone());
        }
    } else if is_remove {
        if already_has_remove.is_some() {
            // do nothing
        } else if let Some(idx) = already_has_add {
            let _ = issue.label_edits.remove(idx);
        } else {
            issue.label_edits.push(edit.clone());
        }
    } else {
        unreachable!()
    }
}
