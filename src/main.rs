use clap::Parser;
use gitlab::api::projects::issues::EditIssue;
use gitlab::api::Query;
use tabled::{Style, Table};
use tap::prelude::*;
use tracing_subscriber::filter::LevelFilter;
use tracing_subscriber::prelude::*;
use tracing_tree::HierarchicalLayer;

mod issue;
mod label;
mod state;

#[cfg(tui)]
mod ui;

#[cfg(not(tui))]
mod ui {
    pub(super) fn start() {}
}

#[derive(clap::Parser, Debug)]
enum Opt {
    /// Download new issue state from the network.
    Download,

    /// Upload the new issue state to the network. Can upload either all changes or only the
    /// specified issues.
    Upload {
        #[clap(value_parser = issue::parse_issue)]
        issues: Vec<issue::IssueId>,
    },

    /// Assign an issue to one or more users.
    Assign {
        #[clap(value_parser = issue::parse_issue)]
        issue: issue::IssueId,
        users: Vec<String>,
    },

    /// Unassign an issue.
    Unassign {
        #[clap(value_parser = issue::parse_issue)]
        issue: issue::IssueId,
    },

    /// Modify an issues labels.
    #[clap(allow_hyphen_values = true)]
    ModifyLabels {
        /// Whether to create new labels if they haven't been seen before.
        #[clap(long)]
        allow_new_labels: bool,
        /// The issue (name or ID).
        #[clap(value_parser = issue::parse_issue)]
        issue: issue::IssueId,
        /// One or more labels to add/remove on the format `+label-to-add -label-to-remove`.
        #[clap(value_parser = label::parse_label_edit)]
        edits: Vec<label::LabelEdit>,
    },

    /// Show the difference between the currently known state of issues and what would be the
    /// result of the queued changes.
    Diff {
        /// Only list the issues and the number of changes, not every single change.
        #[clap(long)]
        summary: bool,
    },

    /// Show what we now about an issue.
    Show {
        #[clap(value_parser = issue::parse_issue)]
        issue: issue::IssueId,
    },

    /// List all downloaded issues
    List {
        #[clap(long, value_parser = parse_filter)]
        filter: Option<Filter>,
        #[clap(long, value_parser = parse_sort)]
        sort: Option<Sort>,
    },

    /// Enter the terminal user interface.
    Tui,
}

#[derive(Clone, Debug)]
struct Filter;

fn parse_filter(_: &str) -> Result<Filter, String> {
    todo!()
}

#[derive(Clone, Debug)]
struct Sort;

fn parse_sort(_: &str) -> Result<Sort, String> {
    todo!()
}

fn download() {
    let token = std::env::var("GITLAB_TOKEN").unwrap();
    let client = gitlab::Gitlab::new("gitlab.com", token).unwrap();
    let issues = gitlab::api::issues::ProjectIssues::builder()
        .project("sornas/test-issues")
        .build()
        .unwrap();
    let issues: Vec<gitlab::Issue> = gitlab::api::paged(issues, gitlab::api::Pagination::All)
        .query(&client)
        .unwrap();
    let labels = gitlab::api::projects::labels::Labels::builder()
        .project("sornas/test-issues")
        // FIXME: .include_ancestor_groups(true) ?
        .build()
        .unwrap();
    let labels: Vec<gitlab::Label> = gitlab::api::paged(labels, gitlab::api::Pagination::All)
        .query(&client)
        .unwrap();
    let issues = issues.into_iter().map(issue::IssueState::new).collect();
    let state = state::State { issues, labels };
    std::fs::write("issues.ron", &ron::to_string(&state).unwrap()).unwrap();
}

fn upload(issues: &[issue::IssueId]) {
    if issues.is_empty() {
        upload_everything();
    } else {
        issues.iter().for_each(upload_changes);
    }
}

fn upload_everything() {
    todo!();
}

fn upload_changes(issue: &issue::IssueId) {
    // FIXME: Check if there are no changes since it won't run otherwise.

    let mut state = state::State::read("issues.ron");
    let issue = state.from_id_mut(issue).unwrap();
    let mut edit_issue = EditIssue::builder();
    edit_issue
        .issue(issue.gitlab_issue.iid.value())
        .project("sornas/test-issues");

    for edit in &issue.label_edits {
        match edit {
            label::LabelEdit::Add(label) => edit_issue.add_label(label),
            label::LabelEdit::Remove(label) => edit_issue.remove_label(label),
        };
    }

    let endpoint = edit_issue.build().unwrap();
    let token = std::env::var("GITLAB_TOKEN").unwrap();
    let client = gitlab::Gitlab::new("gitlab.com", token).unwrap();
    let updated_issue = gitlab::api::raw(endpoint)
        .query(&client)
        .unwrap()
        .pipe_as_ref(String::from_utf8_lossy)
        .pipe_as_ref(serde_json::from_str)
        .unwrap();

    issue.gitlab_issue = updated_issue;
}

fn assign(_issue: &issue::IssueId, _users: &[String]) {
    todo!();
}

fn unassign(_issue: &issue::IssueId) {
    todo!();
}

fn diff(_summary: bool) {
    todo!();
}

fn show(_issue: &issue::IssueId) {
    todo!();
}

fn list(_filter: Option<&Filter>, _sort: Option<&Sort>) {
    let state = state::State::read("issues.ron");
    let issues: Vec<issue::IssueRepr> = state
        .issues
        .iter()
        .filter(|issue| issue.gitlab_issue.state != gitlab::IssueState::Closed)
        .map(|issue| issue.gitlab_issue.clone().into())
        .collect();
    // FIXME sort?
    let table = Table::new(issues).with(Style::rounded()).to_string();
    println!("{table}");
}

fn main() {
    dotenv::dotenv().unwrap();

    let layer = HierarchicalLayer::new(2)
        .with_targets(true)
        .with_filter(LevelFilter::DEBUG);

    tracing_subscriber::registry().with(layer).init();

    let opt = Opt::parse();

    match &opt {
        Opt::Download => download(),
        Opt::Upload { issues } => upload(issues),
        Opt::Assign { issue, users } => assign(issue, users),
        Opt::Unassign { issue } => unassign(issue),
        Opt::ModifyLabels {
            allow_new_labels,
            issue,
            edits,
        } => label::modify_labels(issue, edits, *allow_new_labels),
        Opt::Diff { summary } => diff(*summary),
        Opt::Show { issue } => show(issue),
        Opt::List { filter, sort } => list(filter.as_ref(), sort.as_ref()),
        Opt::Tui => ui::start(),
    }
}
