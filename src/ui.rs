use crossterm::terminal::{
    disable_raw_mode, enable_raw_mode, EnterAlternateScreen, LeaveAlternateScreen,
};
use std::io;
use tui::{
    backend::CrosstermBackend,
    layout::{Constraint, Direction, Layout},
    widgets::{Block, Borders, Row, Table, TableState, Widget},
    Terminal,
};

impl crate::IssueRepr {
    fn into_row<'a>(self) -> Row<'a> {
        Row::new(vec![
            self.id.to_string(),
            self.title,
            self.labels.to_string(),
        ])
    }
}

pub fn start() {
    let mut stdout = io::stdout();
    crossterm::execute!(stdout, EnterAlternateScreen).unwrap();
    enable_raw_mode().unwrap();

    let res = std::panic::catch_unwind(run);

    disable_raw_mode().unwrap();
    crossterm::execute!(stdout, LeaveAlternateScreen).unwrap();

    // Unwrap after leaving the screen so it renders correctly.
    res.unwrap();
}

fn run() {
    let stdout = io::stdout();
    let backend = CrosstermBackend::new(stdout);
    let mut terminal = Terminal::new(backend).unwrap();
    loop {
        terminal
            .draw(|f| {
                let rects = Layout::default()
                    .constraints([Constraint::Percentage(100)].as_ref())
                    .margin(5)
                    .split(f.size());

                let crate::State { issues } =
                    ron::from_str(&std::fs::read_to_string("issues.ron").unwrap()).unwrap();
                let issues: Vec<Row> = issues
                    .into_iter()
                    .map(|issue| crate::IssueRepr::from(issue.gitlab_issue).into_row())
                    .collect();
                let table = Table::new(issues).widths(&[
                    Constraint::Percentage(20),
                    Constraint::Percentage(60),
                    Constraint::Percentage(20),
                ]);

                f.render_stateful_widget(table, rects[0], &mut TableState::default());
                // let block = Block::default()
                //     .title("Block")
                //     .borders(Borders::ALL);
                // f.render_widget(block, size);
            })
            .unwrap();
        std::thread::sleep(std::time::Duration::from_millis(100));
    }
}
