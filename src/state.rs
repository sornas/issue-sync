use serde::{Deserialize, Serialize};
use std::path::{Path, PathBuf};

use crate::issue;

/// State stored between runs for internal usage.
#[derive(Default, Debug, Deserialize, Serialize)]
#[serde(default)]
pub struct State {
    pub issues: Vec<issue::IssueState>,
    // FIXME: Group labels?
    pub labels: Vec<gitlab::Label>,
}

impl State {
    pub fn read(path: impl AsRef<Path>) -> StateRef {
        StateRef::read(path)
    }
}

#[derive(Debug)]
pub struct StateRef {
    state: State,
    path: PathBuf,
}

impl Drop for StateRef {
    fn drop(&mut self) {
        if let Err(e) = self.try_write(&self.path) {
            eprintln!("error storing state: {}", e);
        }
    }
}

impl std::ops::Deref for StateRef {
    type Target = State;

    fn deref(&self) -> &Self::Target {
        &self.state
    }
}

impl std::ops::DerefMut for StateRef {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.state
    }
}

impl StateRef {
    pub fn read(path: impl AsRef<Path>) -> Self {
        let state = ron::from_str(&std::fs::read_to_string(&path).unwrap()).unwrap();
        Self {
            state,
            path: path.as_ref().to_path_buf(),
        }
    }

    pub fn try_write(&self, path: impl AsRef<Path>) -> Result<(), String> {
        let s = ron::to_string(&self.state).map_err(|e| e.to_string())?;
        std::fs::write(path, &s).map_err(|e| e.to_string())
    }

    pub fn from_id(&self, id: &issue::IssueId) -> Option<&issue::IssueState> {
        self.issues
            .iter()
            .find(|issue| issue.gitlab_issue.iid.value() == id.0)
    }

    pub fn from_id_mut(&mut self, id: &issue::IssueId) -> Option<&mut issue::IssueState> {
        self.issues
            .iter_mut()
            .find(|issue| issue.gitlab_issue.iid.value() == id.0)
    }
}
