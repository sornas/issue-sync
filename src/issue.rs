use serde::{Deserialize, Serialize};
use std::fmt;
use tabled::Tabled;
use tap::prelude::*;
use tracing::{event, Level};

use crate::label;
use crate::state;

#[derive(Debug, Clone)]
pub struct IssueId(pub u64);

#[tracing::instrument]
pub fn parse_issue(s: &str) -> Result<IssueId, String> {
    s.parse()
        .map(IssueId)
        .tap(|issue| event!(Level::DEBUG, ?issue))
        .or_else(|_| {
            lookup_issue(s)
                .ok_or(format!("'{}' isn't a known issue", s))
                .tap(|issue| event!(Level::DEBUG, ?issue))
        })
}

pub fn lookup_issue(name: &str) -> Option<IssueId> {
    let state = state::State::read("issues.ron");
    state.issues.iter().find_map(|issue| {
        issue
            .gitlab_issue
            .title
            .eq(name)
            .then_some(IssueId(issue.gitlab_issue.iid.value()))
    })
}

#[derive(Debug, Deserialize, Serialize)]
pub struct IssueState {
    pub gitlab_issue: gitlab::Issue,
    pub label_edits: Vec<label::LabelEdit>,
    /// Outer Option represents if it should be changed. Inner option represents the assignee,
    /// which means None => remove current assignee.
    pub assigned: Option<Option<String>>,
    pub closed: Option<bool>,
}

impl IssueState {
    pub fn new(gitlab_issue: gitlab::Issue) -> Self {
        Self {
            gitlab_issue,
            label_edits: Vec::new(),
            assigned: None,
            closed: None,
        }
    }
}

#[derive(Debug, PartialEq)]
pub struct Labels(pub Vec<String>);

impl fmt::Display for Labels {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let mut labels = self.0.clone();
        labels.sort();
        write!(f, "{}", labels.join(", "))
    }
}

/// What we care about when it comes to issues.
///
/// Used to generate diff reports without lots of information we don't care about.
#[derive(Debug, PartialEq, Tabled)]
pub struct IssueRepr {
    id: u64,
    title: String,
    labels: Labels,
}

impl From<gitlab::Issue> for IssueRepr {
    fn from(issue: gitlab::Issue) -> IssueRepr {
        IssueRepr {
            id: issue.iid.value(),
            title: issue.title,
            labels: Labels(issue.labels),
        }
    }
}
